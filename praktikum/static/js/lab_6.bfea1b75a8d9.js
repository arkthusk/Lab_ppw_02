//chatbox
$('textarea#press').keypress(function(event){
	if(event.keyCode == 13 && !event.shiftKey){
		event.preventDefault();
		var input = $('textarea#press').val()+"<br>";
		$('div.msg-insert').append(input);
		document.getElementById("press").value = "";
	}
});


// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  console.log(print.value)
  if (x === 'ac') {
    print.value = "";
    erase = false;
  } else if (x === 'eval') {
      console.log(print.value)
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}



$(document).ready(function() {
 //inisiasi data tema
  var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
	var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
  
  //default theme jika web pertama kali dibuka
  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }
  //simpan data tema di local storage
  localStorage.setItem('themes', JSON.stringify(themes));
	
  //menload tema ke select2
	var retrievedObject = localStorage.getItem('themes');
	$('.my-select').select2({data: JSON.parse(retrievedObject)});
	
  //menerapkan selectedTheme yang ada di local storage
  var retrievedSelected = JSON.parse(localStorage.getItem('selectedTheme'));
    var key;
    var bcgColor;
    var fontColor;
	for (key in retrievedSelected) {
    	if (retrievedSelected.hasOwnProperty(key)) {
        	bcgColor=retrievedSelected[key].bcgColor;
        	fontColor=retrievedSelected[key].fontColor;
    	}
	}  
	$("body").css({"background-color": bcgColor});
	$("footer").css({"color":fontColor});


  //fungsi tombol apply
	$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var valueTheme = $('.my-select').val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    var theme;
    var a;
    var selectedTheme = {};
    //mencari tema yang sesuai dengan id
    for(a in themes){
    	if(a==valueTheme){
    		var bcgColor = themes[a].bcgColor;
    		var fontColor = themes[a].fontColor;
    		var text = themes[a].text;
    		$("body").css({"background-color": bcgColor});
			  $("footer").css({"color":fontColor});
    		selectedTheme[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
    		localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
    	}
    }
});


});

// //=========change theme=================//
// //inisiasi data tema
// var libraryTema = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
//     {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
//     {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
//     {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
//     {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
//     {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
//     {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
//     {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
//     {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
//     {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
//     {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}];
// //yg dipilih ketika pertama dibuka
// var temaYangDipilih = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

// //men set tema ketika pertama kali dibuka
// if(localStorage.getItem("temaYangDipilih")===null){
// 	localStorage.setItem('temaYangDipilih',JSON.stringify(temaYangDipilih));
// }
// //simpan data tema
// localStorage.setItem('libraryTema',JSON.stringify(libraryTema));

// //load tema yang di pilih
// var object = localStorage.getItem('libraryTema');
// $('.my-select').select2({data : JSON.parse(object)});

// //menerapkan tema
// var ambilPilihan = JSON.parse(localStorage.getItem('temaYangDipilih'));
// 	var kunci;
// 	var warnaBG;
// 	var warnaFont;
// 	for(kunci in ambilPilihan){
// 		if(ambilPilihan.hasOwnProperty(kunci)){
// 			warnaBG = ambilPilihan[kunci].warnaBG;
// 			warnaFont = ambilPilihan[kunci].warnaFont;	
// 		}
// 	}
// 	$("body").css({"background-color" : warnaBG});
// 	$("footer").css({"color" : warnaFont});

// //fungsi untuk tombol apply
// $('.apply-button-class').on('click', function(){  // sesuaikan class button
//     // [TODO] ambil value dari elemen select .my-select
//     // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
//     // [TODO] ambil object theme yang dipilih
//     // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
//     // [TODO] simpan object theme tadi ke local storage selectedTheme
//     var theme;
//     var x;
//     var temaYangDipilih = {};
//     //cari tema lewat id
//     for(x in libraryTema){
//     	if(x == $('.my-select').val() ){
//     		var bcgColor = libraryTema[x].bcgColor;
//     		var fontColor = libraryTema[x].fontColor;
//     		var text = libraryTema[x].text;
//     		$("body").css({"background-color":bcgColor});
//     		$("footer").css({"color":fontColor});
//     		temaYangDipilih[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
//     		localStorage.setItem('temaYangDipilih',JSON.stringify(temaYangDipilih));
//     	}
//     }
// 	});
