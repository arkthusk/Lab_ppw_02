from django.shortcuts import render

response = {}

# Create your views here.

def index(request):
    html = 'lab_6/lab_6.html'
    response['author'] = "Aji Wuryanto"
    return render(request, html, response)