from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,friend_list,add_friend,delete_friend,validate_npm,model_to_dict
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
import requests
import urllib
import os
import environ
class Lab7UnitTest(TestCase):

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_paginator(self):
		data = ['a','b','c','d','e','f']
		paginator = Paginator(data, 2)
		page1 = paginator.page(1)
		page2 = paginator.page(2)
		page3 = paginator.page(3)
		self.assertEqual(page1.object_list, list(data[0:2]))
		self.assertEqual(page2.object_list, list(data[2:4]))
		self.assertEqual(page3.object_list, list(data[4:6]))

	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})

		
