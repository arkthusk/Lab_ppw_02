from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
import requests
from django.urls import reverse
import environ
from .omdb_api import get_detail_movie, create_json_from_dict, search_movie, get_api_key


root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
# Create your tests here.
class Lab10UnitTest(TestCase):

	def test_lab_10_url_is_exist(self):
		response = Client().get('/lab-10/')
		self.assertEqual(response.status_code, 200)

	def test_lab10_using_index_func(self):
		found = resolve('/lab-10/')
		self.assertEqual(found.func, index)



	def test_list_movie_page_exist(self):
		#login
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.get(reverse('lab-10:movie_list'))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post,"lab_10/list.html")
		response_post = self.client.get(reverse('lab-10:movie_list'),{'judul':'It','tahun':'2017'})
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post,"lab_10/list.html")

		#logout
		
	def test_detail_page(self):
		#test jika tidak login (tidak ada key 'user_login' di session)
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id':'tt1396484'}))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post,"lab_10/detail.html")
		#login
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.get(reverse('lab-10:dashboard'))
		response_post = self.client.get(reverse('lab-10:movie_detail', kwargs={'id':'tt1396484'}))
		self.assertEqual(response_post.status_code, 200)
		self.assertTemplateUsed(response_post,"lab_10/detail.html")
		#logout
		
	
		


	def test_search_movie_exist(self):
		response_post = self.client.get(reverse('lab-10:api_search_movie', kwargs={'tahun':'-','judul':'It'}))
		self.assertEqual(response_post.status_code, 200)
		response_post = self.client.get(reverse('lab-10:api_search_movie', kwargs={'judul':'-','tahun':'2017'}))
		self.assertEqual(response_post.status_code, 200)
		response_post = self.client.get(reverse('lab-10:api_search_movie', kwargs={'judul':'It','tahun':'2017'}))
		self.assertEqual(response_post.status_code, 200)
		response_post = self.client.get(reverse('lab-10:api_search_movie', kwargs={'judul':'-','tahun':'-'}))
		self.assertEqual(response_post.status_code, 200)


	#######################################################################
	# test omdb_api

	def test_get_api_key(self):
		self.assertEqual(get_api_key(),"bed722b5")

	def test_get_detail_movie(self):
		response = requests.get("http://www.omdbapi.com/?i=tt1396484&apikey="+get_api_key())
		response = response.json()
		response = create_json_from_dict(response)
		self.assertEqual(response,get_detail_movie("tt1396484"))

	def test_search_non_exist_movie(self):
		response = search_movie("asdf","-")
		self.assertEqual(len(response),0)

	def test_search_exist_movie(self):
		response = search_movie("Kingsman: The Golden Circle","2017")
		self.assertTrue(len(response)>0)

	###########################################################################
	# test utils


